/*
 * @Description:
 * @Author: 晋龍
 * @Date: 2022-12-18 19:37:57
 * @LastEditors: 晋龍
 * @LastEditTime: 2023-01-16 15:41:57
 */
/// <reference types="vite/client" />
declare module '*.vue' {
    import type { DefineComponent } from 'vue'
    // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
    const component: DefineComponent<{}, {}, any>
    export default component
  }
