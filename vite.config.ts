/*
 * @Author: Badcandy 568197314@qq.com
 * @Date: 2022-12-18 14:43:05
 * @LastEditors: “badCandy” “568197314@qq.com”
 * @LastEditTime: 2023-02-12 11:50:06
 * @FilePath: /dcollege/vite.config.ts
 * @Description: 
 * 
 * Copyright (c) 2022 by Badcandy 568197314@qq.com, All Rights Reserved. 
 */
import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import components from 'unplugin-vue-components/vite'
import autoImport from 'unplugin-auto-import/vite'
import {VarletUIResolver} from 'unplugin-vue-components/resolvers'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: {
        transformAssetUrls: {
          'var-image': ['src']
        }
      }
    }),
    components({
      resolvers:[VarletUIResolver()]
    }),
    autoImport({
      resolvers: [VarletUIResolver({autoImport:true})]
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
  // server: {
  //   proxy: {
  //     "/digital/college": {
  //       target: "http://8.142.107.97:8909",
  //       changeOrigin: true,
  //       // rewrite:(path) => path.replace(/\^\/gmy/,'')
  //     }
  //   }
  // }
})
