<!--
 * @Author: Badcandy 568197314@qq.com
 * @Date: 2022-12-18 14:43:05
 * @LastEditors: Badcandy 568197314@qq.com
 * @LastEditTime: 2022-12-18 19:23:01
 * @FilePath: \dcollege\README.md
 * @Description: 
 * 
 * Copyright (c) 2022 by Badcandy 568197314@qq.com, All Rights Reserved. 
-->
# dcollege

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```
### 请求例子，所有请求都应在`src/services`文件夹下封装成一个方法暴露出去
```typescript
import { Request } from "@/utils/axios";

var request = new Request({});

export function AxiosExample(postData:{user_name:string|number,password:string|number}) {
    return request.post('/api/example',postData)
}
```
### git上传规范
```
feat:          新增feature

fix:           修复bug

docs:          仅仅修改了文档，比如README, CHANGELOG, CONTRIBUTE等等            

style:         仅仅修改了空格、格式缩进、标点符号等等，不改变代码逻辑

refactor:      代码重构，没有加新功能或者修复bug

test:          测试用例，包括单元测试、集成测试等            

chore:         改变构建流程、或者增加依赖库、工具等

perf:          优化相关，比如提升性能、体验

revert:        回滚到上一个版本
```
### 状态码
```
20000     请求成功
50000     业务异常
40010     认证异常！
```