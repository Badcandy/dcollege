

import { defineStore } from 'pinia'
import { computed, ref } from 'vue';
import { toRaw } from '@vue/reactivity';
import axios from '../utils/axios'
import { document } from '../services/submit'

export const useSubStore = defineStore('subStore', () => {

    const sub = async (data) => {
        const res = await document(data)
        return res
    }
    return {
        sub
    }
})
