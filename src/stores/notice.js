import { defineStore } from "pinia";
import { computed, ref } from 'vue';

import { getUserNotice } from '@/services/notice'

export const useNoticeStore = defineStore('noticeStore', () => {
    // 通知信息
    const noticeInfo = ref()
    // 用户获取当前课程的通知
    const getLessonNotice = (courseId) => {
        getUserNotice(courseId).then(res => {
            // console.log(res);
            noticeInfo.value = res.data.data.content
            // console.log('noticeInfo',noticeInfo);
        }).catch(err => {
            console.log(err);
        })
    }

    return {
        noticeInfo,
        getLessonNotice
    }
})