/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: Wupy
 * @Date: 2023-02-20 11:17:44
 * @LastEditors: Wupy
 * @LastEditTime: 2023-02-21 14:15:38
 */
import { defineStore } from "pinia";
import { computed, ref } from 'vue';

import {getDiscussInfo,addComment} from '@/services/discuss'

export const useDiscussStore = defineStore('discussStore',()=>{
    // 课题信息
    const discussList = ref([])
    // 评论列表
    const commentList = ref([])
    // 获取讨论信息
    const DisucssList = (topicId)=>{
        console.log('222',topicId);
        getDiscussInfo(topicId).then(res=>{
            discussList.value = res.data.data.topic
            commentList.value = res.data.data.topicRelListVo
        })
    }
    // 发布评论
    const addUserComment = (topicId,userId,content,createTime)=>{
        addComment(topicId,userId,content,createTime).then(res=>{
            console.log('addComment',res);
            if(res.data.code === 20000){
                Snackbar.success('评论成功')
                location.reload()
                DisucssList(topicId)
            }else{
                Snackbar.error('评论失败')
            }
        })
    }

    return{
        discussList,commentList,
        DisucssList,addUserComment,
    }
} )