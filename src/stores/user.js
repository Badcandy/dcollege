/*
 * @Description: 
 * @Author: 晋龍
 * @Date: 2023-02-11 17:52:16
 * @LastEditors: 晋龍
 * @LastEditTime: 2023-02-18 21:55:42
 */
import { defineStore } from 'pinia'
import { computed, ref } from 'vue';
import { toRaw } from '@vue/reactivity';
import axios from '../utils/axios'
import { login, register, getUser, bind, getCollege,addCollege,getCollegeId} from '../services/user'

export const useUserStore = defineStore('userStore', () => {
    // state
    let user = ref({})
    let college = ref({})
    let gid = ref({})
    // 用户登录
    const loginCheck = (mobileNumber, password) => {
        const res = login(mobileNumber, password)
        return res
    }
    // 注册用户
    const registerCheck = async (mobileNumber, password, name,userType) => {
        const res = await register(mobileNumber, password, name,userType)
        return res
    }
    // 获取用户信息
    const getuser = async () => {
        const res = await getUser()
        // let newUser = {}
        // newUser = res.data.data
        user.value = res.data.data
        // for (let item in newUser) {
        //     if (!newUser[item]) {

        //         newUser[item] = ''
        //     }
        // }
        // user.value = JSON.parse(JSON.stringify(newUser))
        return user
    }
    // 用户绑定学号
    const bindUser = (collegeId, userNumber) => {
        const res = bind(collegeId, userNumber)
        return res
    }
    // 获取所有学院
    const getAllCollege = async() => {
        const res = await getCollege()
        college.value = res.data.data
        return res
    }
    // 添加学院
    const addThisCollege = async(userNumber,collegeId) =>{
        const res = await addCollege(userNumber,collegeId)
        return res
    }
    // 获取对应的学院信息
    const getId = async(id)=>{
        const res = await getCollegeId(id)
        gid.value = res.data.data
        return res
    }
    return {
        college,
        gid,
        user,
        loginCheck,
        registerCheck,
        getuser,
        bindUser,
        getAllCollege,
        addThisCollege,
        getId
    }
})