/*
 * @Description: 
 * @Author: 晋龍
 * @Date: 2023-02-19 16:15:49
 * @LastEditors: 晋龍
 * @LastEditTime: 2023-02-21 20:21:24
 */
import { defineStore } from 'pinia'
import { computed, ref } from 'vue';
import axios from '../utils/axios'
import { getWorkTime,getWorkAllTime,addWorkTime} from '../services/workload'

export const workTImeStore = defineStore('WorkTimeStore', () => {
    // state
    const workTime = ref({})
    // 获取当前课程工作量
    const workTimeAllGet = async()=>{
        const res = await getWorkAllTime()
        console.log(res.data.data);
        workTime.value = res.data.data
    }
    // 获取当前课程工作量
    const workTimeGet = async(courseId)=>{
        const res = await getWorkTime(courseId)
        workTime.value = res.data.data
        return res.data.message
    }
    // 提交课时
    const workTimeAdd = async(courseId,experKs,experXs,grade,theoryKs,theoryXs,totalKs,userId)=>{
        const res = await addWorkTime(courseId,experKs,experXs,grade,theoryKs,theoryXs,totalKs,userId)
    }
    return {
        workTime,
        workTimeGet,
        workTimeAdd,
        workTimeAllGet
    }
})