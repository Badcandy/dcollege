/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: Wupy
 * @Date: 2023-02-18 20:27:43
 * @LastEditors: Wupy
 * @LastEditTime: 2023-02-21 14:31:31
 */
import { defineStore } from "pinia";
import { computed, ref } from 'vue';

import { getLessonList, getLessonInfo, stuSign, getSectionInfo,getLessonNameInfo } from '@/services/lesson'

export const useLessonStore = defineStore('lessonStore', () => {
    // 课程列表
    const lessonList = ref([])
    const lessonInfo = ref([])
    const sectionInfo = ref()
    const lessonName = ref([])
    // 课程信息
    // 获取课程列表
    const LessonList = () => {
        getLessonList().then(res => {
            lessonList.value = res.data.data
            // console.log(lessonList);
        })
    }
    // 获取当前课程的章节
    const LessonInFo = (courseId) => {
        getLessonInfo(courseId).then(res => {
            // console.log(res);
            lessonInfo.value = res.data.data
        })
    }
    // 签到方法!!!
    const userSign = (userId, createTime) => {
        return stuSign(userId, createTime)
    }
    // 当前章节信息
    const getSectionBySectionId = (sectionId) => {
        getSectionInfo(sectionId).then(res => {
            sectionInfo.value = res.data.data
        })
    }
    //获取当前课程的名字讨论信息
    const getLessonName = (courseId)=>{
        getLessonNameInfo(courseId).then(res => {
            console.log('名字',res);
            lessonName.value = res.data.data
        })
    }
    return {
        lessonList, lessonInfo, sectionInfo,lessonName,
        LessonList, LessonInFo, userSign, getSectionBySectionId,getLessonName
    }
})