

import { defineStore } from "pinia";
import { computed, ref } from "vue";

import { getCourseList } from "@/services/course";

export const useCourseStore = defineStore("CourseStore", () => {
  // 日程列表
  let courseList = ref([]);

  const userCourse = () => {
    getCourseList().then((res) => {
      // console.log("data", res.data);
      courseList.value = res.data;
    });
  };

  return {
    courseList,
    userCourse,
  };
});
