import { defineStore } from "pinia";
import { computed, ref } from "vue";

import { getUserNotice } from "@/services/noticedetails";

export const useUserNoticeStore = defineStore("userNoticeStore", () => {
  const noticeContent = ref([]);

  const userNotice = () => {
    getUserNotice().then((res) => {
      console.log("data", res.data);
      noticeContent.value = res.data;
    });
  };
  return {
    noticeContent,
    userNotice,
  };
});
