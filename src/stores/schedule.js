/*
 * @Author: badcandy 568197314@qq.com
 * @Date: 2023-02-16 15:24:53
 * @LastEditors: badcandy 568197314@qq.com
 * @LastEditTime: 2023-02-20 16:22:32
 * @FilePath: /dcollege/src/stores/schedule.js
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import { defineStore } from "pinia";
import { computed, ref } from 'vue';

import { getUserSchedule, getScheduleListByDay } from "@/services/schedule";

export const useUserScheduleStore = defineStore('userScheduleStore', () => {
    // 日程列表
    let scheduleList = ref([])
    let nowScheduleList = ref([])
    const userSchedule = () => {
        getUserSchedule().then((res) => {
            console.log('data', res.data);
            scheduleList.value = res.data.data
        })
        return  scheduleList;
    }

    const getScheduleList = (date) => { 
        getScheduleListByDay(date).then((res) => { 
            nowScheduleList.value = JSON.parse(JSON.stringify(res.data.data));
            console.log(nowScheduleList.value)
        }).catch((err) => { 
            console.log('error', err)
        })
    }

    return {
        scheduleList,
        nowScheduleList,
        userSchedule,
        getScheduleList
    }
})