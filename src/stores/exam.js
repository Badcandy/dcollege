/*
 * @Description: 考试方法及state
 * @Author: 晋龍
 * @Date: 2023-02-12 13:44:36
 * @LastEditors: 晋龍
 * @LastEditTime: 2023-02-20 21:14:42
 */
import { defineStore } from 'pinia'
import { computed, ref } from 'vue';
import { toRaw } from '@vue/reactivity';
import axios from '../utils/axios'
import { getPaperList, getQuestionList, publishExam, publishPaper, publishQuestion,getExamList,submitExam,getExamDetailByUser,getLessonList } from '../services/exam'

export const useExamStore = defineStore('ExamStore', () => {
    // state
    // 考试列表
    let examList = ref([])
    let examLists = ref([])

    // 试卷列表
    let list = ref({})
    // 获得问题试卷
    let questionList = ref({})
    // 获取当前学生考试试卷
    let examDetail = ref({})
    // 课程列表
    let lessonList = ref([])

    // 获取当前用户的考试列表
    const  ExamList = () =>{
        getExamList().then(res=>{
            console.log('res',res);
            examLists.value = res.data.data
            examList.value = res.data
        })
    }
    // 分页获取试卷列表
    const getExamLists = async () => {
        const res = await getPaperList()
        console.log(res.data.data,"list");
        list.value = res.data.data
    }
    // 获取问题试卷
    const getTheQuestionList = async () => {
        const res = await getQuestionList()
        console.log(res.data.data,"questionList");
        questionList.value = res.data.data
    }
    // 获取当前学生考试试卷
    const getExamDetail = async() =>{
        const res = await getExamDetailByUser(examId)
        console.log(res.data.data,"examDetail");
        examDetail.value = res.data.data
    }
    // 发布考试-考试与试卷关联
    const publishForExam = (courseId,endTime,name,paperId,startTime,teacherName) => {
        const res = publishExam(courseId,endTime,name,paperId,startTime,teacherName)
        console.log(res.data.data);
        return res
    }
    // 发布考试-试卷与问题关联
    const publishForPaper = (name,questionIdList) => {
        const res = publishPaper(name,questionIdList)
        console.log(res.data.data);
        return res
    }
    // 创建问题
    const publishForQuestion = (answer,name,option,score,type) => {
        const res = publishQuestion(answer,name,option,score,type)
        console.log(res.data.code);
        return res
    }

    // 提交考试试卷
    const submitExamDetail = ({examDetail,examId,score}) =>{
        console.log(examDetail,examId,score);
        const res = submitExam(examDetail,examId,score)
        console.log(res);
        return res.data

    }
    // 课程信息
    // 获取课程列表
    const LessonList = async()=>{
        const res = await getLessonList()
        lessonList.value = res.data.data
    }
    return {
        examList, 
        list,
        questionList,
        examDetail,
        lessonList,
        examLists,
        ExamList,
        getExamLists, 
        getTheQuestionList,
        publishForExam, 
        publishForPaper, 
        publishForQuestion,
        submitExamDetail,
        getExamDetail,
        LessonList
    }
})