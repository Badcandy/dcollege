/*
 * @Author: Badcandy 568197314@qq.com
 * @Date: 2022-12-18 14:43:05
 * @LastEditors: badcandy 568197314@qq.com
 * @LastEditTime: 2023-02-21 20:55:42
 * @FilePath: /dcollege/src/router/index.ts
 * @Description: 
 * 
 * Copyright (c) 2022 by Badcandy 568197314@qq.com, All Rights Reserved. 
 */
import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import TeachHomeView from '../views/TeachHomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: () => import('../views/IndexPage.vue')
    },
    {
      path: '/lesson',
      name: 'lesson',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/lesson/lesson.vue')
    },
    {
      path: '/lessonDetail',
      name: 'lessonDetail',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/lessonDetail/lessonDetail.vue'),
      children: [
        
        {
          path: '/lessonChapter',
          name: 'lessonChapter', meta: {
            requireAuth: true
          },
          component: () => import('../views/lessonDetail/lessonChapter.vue')
        },
        {
          path: '/lessonDiscuss',
          name: 'lessonDiscuss',
          meta: {
            requireAuth: true
          },
          component: () => import('../views/lessonDetail/lessonDiscuss.vue')
        },
        {
          path: '/lessonDetail',
          redirect: '/lessonChapter'
        },

        

      ]
    },
    {
      path: '/discussDetail',
      name: 'discussDetail',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/lessonDetail/discussDetail.vue')
    },
    {
      path: '/addDiscuss',
      name: 'addDiscuss',
      component: () => import('../views/lessonDetail/addDiscuss.vue')
    },
    {
      path: '/classDetail',
      name: 'classDetail',
      component: () => import('../views/lessonDetail/classDetail.vue')
    },
    {
      path: '/main',
      name: 'main',
      component: HomeView,
      meta: {
        requireAuth: true
      },
      children: [
        {
          path: '/home',
          name: 'home',
          meta: {
            requireAuth: true
          },
          component: () => import('../views/home/home.vue')
        },
        {
          path: '/contact',
          name: 'contact',
          meta: {
            requireAuth: true
          },
          component: () => import('../views/contact/contact.vue')
        },
        {
          path: '/message',
          name: 'message',
          meta: {
            requireAuth: true
          },
          component: () => import('../views/message/message.vue')
        },
        {
          path: '/personal',
          name: 'personal',
          meta: {
            requireAuth: true
          },
          component: () => import('../views/personal/personal.vue')
        },

        {
          path: '/mystatus',
          name: 'mystatus',
          component: () => import('../views/personal/mystatus.vue')
        },
        {
          path: '/college',
          name: 'college',
          component: () => import('../views/contact/college.vue')
        },
        {
          path: '/addfriend',
          name: 'addfriend',
          component: () => import('../views/contact/addfriend.vue')
        },
        {
          path: '/userdata',
          name: 'userdata',
          component: () => import('../views/contact/userdata.vue')
        },
        {
          path: '/mydata',
          name: 'mydata',
          component: () => import('../views/personal/mydata.vue')
        },
        {
          path: '/main',
          redirect: '/home'
        },

      ]
    },
    {
      path: '/teachMain',
      name: 'teachmain',
      component: TeachHomeView,
      meta: {
        requireAuth: true
      },
      children: [
        {
          path: '/teachHome',
          meta: {
            requireAuth: true
          },
          component: () => import('../views/teachHome/teachHome.vue')
        },
        {
          path: '/teachmain',
          redirect: '/teachHome',
          meta: {
            requireAuth: true
          },
        },]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        requireAuth: false
      },
      component: () => import('../views/login/login.vue')
    },
    {
      path: '/register',
      name: 'register',
      meta: {
        requireAuth: false
      },
      component: () => import('../views/login/register.vue')
    },
    {
      path: '/bind',
      name: 'bind',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/login/bind.vue')
    },
    {
      path: '/myWorkload',
      name: 'myWorkload',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/workload/myWorkload.vue')
    },
    {
      path: '/reporting',
      name: 'reporting',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/workload/reporting.vue')
    },
    {
      path: '/setup/aboutdco',
      name: 'aboutdco',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/personal/setup/aboutdco.vue')
    },
    {
      path: '/setup/account',
      name: 'account',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/personal/setup/account.vue')
    },
    {
      path: '/setup/feedback',
      name: 'feedback',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/personal/setup/feedback.vue')
    },
    {
      path: '/setup/name',
      name: 'name',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/personal/setup/name.vue')
    },
    {
      path: '/setup',
      name: 'setup',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/personal/setup/setup.vue')
    },
    {
      path: '/misspage',
      name: 'misspage',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/personal/setup/misspage.vue')
    },
    {
      path: '/examListByTeacher',
      name: 'examListByTeacher',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/exam/examListByTeacher.vue')
    },
    {
      path: '/examListByUser',
      name: 'examListByUser',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/exam/examListByUser.vue')
    },
    {
      path: '/publishQuestion',
      name: 'publishQuestion',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/exam/publishQuestion.vue')
    },
    {
      path:'/schedule', 
      name:'schedule',
      meta:{
        requireAuth:true
      },
      component:() => import('../views/schedule/schedule.vue')
    },
    {
      path: '/addToDo',
      name: 'addToDo',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/schedule/addToDo.vue')
    },
    {
    path: '/tiku',
    name: 'tiku',
    children:[
      {
        path: '/task',
        name: 'task',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/task/task.vue')
      },
      {
        path: '/zhenti',
        name: 'zhenti',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/task/zhenti.vue')
      },
      {
        path: '/dlpage',
        name: 'dlpage',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/task/dlpage.vue')
      },
      {
        path: '/zuoye',
        name: 'zuoye',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/task/zuoye.vue')
      },
      {
        path: '/search',
        name: 'search',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/task/search.vue')
      },
    ]
  },
  {
    path: '/paper',
    name: 'paper',
    children:[
      {
        path: '/xuanti',
        name: 'xuanti',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/paper/paper.vue')
      },
      {
        path: '/list',
        name: 'list',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/paper/list.vue')
      },
      {
        path: '/details',
        name: 'details',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/paper/details.vue')
      },
      {
        path: '/presubmit',
        name: 'presubmit',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/paper/presubmit.vue')
      },
      {
        path: '/submit',
        name: 'submit',
        meta: {
          requireAuth: true
        },
        component:()=>import('../views/paper/submit.vue')
      },
    ]
  },
  {
    path: '/notice',
    name: 'notice',
    meta: {
      requireAuth: true
    },
    component:()=>import('../views/notice/notice.vue')
  },
  {
      path: '/chat',
      name: 'chat',
      meta: {
        requireAuth: true
      },
      component: () => import('../views/message/chat/chat.vue')
    }
  ]
})
// 路由拦截
router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {
    if (localStorage.getItem('token')) {
      next()
    } else {
      next('/login')
    }
  } else {
    if (localStorage.getItem('token') && to.path == '/login') {
      next()
    } else {
      next()
    }
  }
})

export default router
