/*
 * @Author: Badcandy 568197314@qq.com
 * @Date: 2022-12-18 19:08:40
 * @LastEditors: Badcandy 568197314@qq.com
 * @LastEditTime: 2022-12-18 19:15:36
 * @FilePath: \dcollege\src\services\axios-example.ts
 * @Description: 请求例子
 * 
 * Copyright (c) 2022 by Badcandy 568197314@qq.com, All Rights Reserved. 
 */
import { Request } from "@/utils/axios";

var request = new Request({});

export function AxiosExample(postData:{user_name:string|number,password:string|number}) {
    return request.post('/api/example',postData)
}