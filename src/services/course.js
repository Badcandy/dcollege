


import { Request } from "@/utils/axios";

var request = new Request({});


export const getCourseList = () => {
    return request.get('/schedule/getScheduleListByUser')
}