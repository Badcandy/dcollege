import { Request } from "@/utils/axios";

var request = new Request({});

export const document = (data) => {
    return request.post('/message/sendMessage', {
        data
    })
}