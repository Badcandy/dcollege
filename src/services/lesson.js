/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: Wupy
 * @Date: 2023-02-18 20:22:24
 * @LastEditors: Wupy
 * @LastEditTime: 2023-02-21 14:28:13
 */
import { Request } from "@/utils/axios";

var request = new Request({});

// 获取课程列表
export const getLessonList = ()=>{
    return request.get('/getCourseListByUser')
}

//获取该课程的章节列表
export const getLessonInfo = (courseId)=>{
    return request.get('/getSectionListByCourseId/'+courseId)
}

// 签到方法
export const stuSign = (userId,createTime)=>{
    return request.post('/addUserSignRel',{
        userId,createTime
    })
}
// 获取当前章节的详细信息
export const getSectionInfo = (sectionId)=>{
    return request.get('/getSectionBySectionId/'+sectionId)
}

//获取当前课程的名字讨论信息
export const getLessonNameInfo = (courseId)=>{
    return request.get('/getCourseListByCourseId/'+courseId)
}