/*
 * @Author: badcandy 568197314@qq.com
 * @Date: 2023-02-16 15:24:53
 * @LastEditors: badcandy 568197314@qq.com
 * @LastEditTime: 2023-02-19 19:13:32
 * @FilePath: /dcollege/src/services/schedule.js
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import { Request } from "@/utils/axios";

var request = new Request({});


// 获取日程
export const getUserSchedule = ()=>{
    return request.get('/schedule/getScheduleListByUser')
}

// 获取指定日期的日程
export const getScheduleListByDay = (date) => { 
    return request.get('/schedule/getScheduleListByDay?date=' + date)
}

// 添加日程
export const addSchedule = (data) => { 
    for (let item in data) {
        if (
          item === "name" ||
          item === "location" ||
          item === "scheduleTime" ||
          item === "reminderTime"
        ) {
          if (data[item] === "") {
            return Snackbar.error("请填写完整的日程信息");
          }
        }
      }
    return request.post('/schedule/addSchedule', data)
}