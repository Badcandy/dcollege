/*
 * @Description: 
 * @Author: nxd
 * @Date: 2023-02-19 13:55:21
 * @LastEditors: nxd
 * @LastEditTime: 2023-02-19 19:54:42
 */
import { Request } from "@/utils/axios";
var request = new Request({});
export const addUserIdentity = (name,idNumber) => {
    return request.post('/user/addUserIdentityRel', {
        name, 
        idNumber,
    })
}