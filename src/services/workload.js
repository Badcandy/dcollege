/*
 * @Description: 工作量
 * @Author: 晋龍
 * @Date: 2023-02-19 16:15:40
 * @LastEditors: 晋龍
 * @LastEditTime: 2023-02-19 18:33:19
 */
import { Request } from "@/utils/axios";

var request = new Request({});
// 获取当前用户所有课程工作量
export const getWorkAllTime = () => {
    return request.get('/getAllWorkByUser')
}
// 获取当前课程工作量
export const getWorkTime = (courseId) => {
    return request.get('/getWorkTime'+courseId)
}
// 提交课时
export const addWorkTime = (courseId,experKs,experXs,grade,theoryKs,theoryXs,totalKs,userId) => {
    return request.post('/submitWork', {courseId,experKs,experXs,grade,theoryKs,theoryXs,totalKs,userId})
}