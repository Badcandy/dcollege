/*
 * @Author: badcandy 568197314@qq.com
 * @Date: 2023-02-20 16:28:52
 * @LastEditors: badcandy 568197314@qq.com
 * @LastEditTime: 2023-02-20 16:37:20
 * @FilePath: /dcollege/src/services/message.ts
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import { Request } from "@/utils/axios";

var request = new Request({});

// 获取消息列表
export const getMessageList = () => { 
    return request.get("/message/getMessageListByUser")
}

// 获取当前朋友的消息
export const getMessageByFriend = (friendId:string) => { 
    return request.get("/message/getMessageByFriend?friendId="+friendId)
}

// 发送消息
export const sendMessage = (data:{content: string, friendId: string}) => { 
    return request.post("/message/sendMessage", data)
}