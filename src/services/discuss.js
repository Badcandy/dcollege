/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: Wupy
 * @Date: 2023-02-20 11:03:42
 * @LastEditors: Wupy
 * @LastEditTime: 2023-02-21 14:15:21
 */
import { Request } from "@/utils/axios";

var request = new Request({});


// 查看当前讨论的信息
export const getDiscussInfo = (topicId)=>{
    return request.get('/getTopicByTopicId/'+topicId)
}

// 发布评论
export const addComment = (topicId,userId,content,createTime)=>{
    return request.post('/addTopicByUserAndTopicId',{
        topicId,userId,content,createTime
    })
}