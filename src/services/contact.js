/**
 $ @Author: jyq
 $ @Date: 2023-02-12 15:31:14
 $ @LastEditTime: 2023-02-12 15:32:22
 $ @LastEditors: jyq
 $ @Description: 
 $ @FilePath: \dcollege\src\services\contact.js
 $ @可以输入预定的版权声明、个性签名、空行等
 */
import { Request } from "@/utils/axios";

var request = new Request({});

//获取通讯录朋友
export const getContactfriend = () => {
    return request.get('/contact/getAllFriendByUser')
}
