/*
 * @Description: 考试接口
 * @Author: 晋龍
 * @Date: 2023-02-12 13:44:31
 * @LastEditors: 晋龍
 * @LastEditTime: 2023-02-20 20:50:46
 */
import { Request } from "@/utils/axios";

var request = new Request({});


// 获取当前用户的考试列表
export const getExamList = () => {
    return request.get('/getExamListByUser')
}
// 分页获取试卷列表
export const getPaperList = () =>{ 
    return request.get('/getPaperList')
}
// 获取问题试卷
export const getQuestionList = () =>{ 
    return request.get('/getQuestionList')
}
// 获取当前学生考试试卷
export const getExamDetailByUser = (examId) =>{
    return request.get("/getExamDetailByUser"+examId)
}
// 发布考试-考试与试卷关联
export const publishExam = (courseId,endTime,name,paperId,startTime,status,teacherName) =>{ 
    return request.post('/publishExam',{courseId,endTime,name,paperId,startTime,status,teacherName})
}
// 发布考试-试卷与问题关联
export const publishPaper = (name,questionIdList) =>{ 
    return request.post('/publishPaper',{name,questionIdList})
}
// 创建问题
export const publishQuestion = (answer,name,option,score,type) =>{ 
    return request.post('/publishQuestion',{answer,name,option,score,type})
}
// 提交考试试卷
export const submitExam = (examDetail,examId,score) =>{
    console.log(examDetail,examId,score);
    return request.post('/submitExamDetailByUser',{examDetail,examId,score})
}
// 获取课程列表
export const getLessonList = ()=>{
    return request.get('/getCourseListByUser')
}
var request = new Request({});
