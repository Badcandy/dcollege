/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: Wupy
 * @Date: 2023-02-20 12:32:09
 * @LastEditors: Wupy
 * @LastEditTime: 2023-02-20 13:07:22
 */
import { Request } from "@/utils/axios";

var request = new Request({});

// 获取用户对于该课程的通知
export const getUserNotice = (courseId) =>{
   return request.get('/getNoticeByUserAndCourseId/'+courseId)
}