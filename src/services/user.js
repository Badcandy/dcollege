/*
 * @Description: 
 * @Author: 晋龍
 * @Date: 2023-02-11 17:52:23
 * @LastEditors: 晋龍
 * @LastEditTime: 2023-02-12 20:30:00
 */

// 导出很多发送请求的方法
// export default login = (postData) => {
// }
import { Request } from "@/utils/axios";

var request = new Request({});

export const login = (mobileNumber, password) => {
    return request.post('/login/formLogin', {
        mobileNumber, //手机号
        password //密码
    })
}

export const register = (mobileNumber, password,name,userType) => {
    return request.post('/login/registry', {
        mobileNumber, password,name,userType
    })
}
export const getUser = () =>{
    return request.get('/user/getUserInfo')
}
export const bind = (collegeId,userNumber) =>{
    return request.post('/login/registry',{
        collegeId,userNumber
    })
}
export const getCollege = () =>{
    return request.get('/college/getAllCollege')
}

export const addCollege = (userNumber,collegeId) =>{
    return request.post('/login/bindUserCollege',{
        userNumber,collegeId
    })
}
export const getCollegeId = (id) =>{ 
    return request.get('/college/getCollege/'+id)
}
   
