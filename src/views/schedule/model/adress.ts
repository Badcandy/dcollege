/*
 * @Author: badcandy 568197314@qq.com
 * @Date: 2023-02-19 15:10:33
 * @LastEditors: badcandy 568197314@qq.com
 * @LastEditTime: 2023-02-19 15:19:51
 * @FilePath: /dcollege/src/views/schedule/model/adress.ts
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
const address =[
    {
        label: '勤学楼A',
        value: '勤学楼A'
    },
    {
        label: '勤学楼B',
        value: '勤学楼B'
    },
    {
        label: '勤学楼C',
        value: '勤学楼C'
    },
    {
        label: '勤学楼D',
        value: '勤学楼D'
    },
    {
        label: '3号楼',
        value: '3号楼'
    },
    {
        label: '4号楼',
        value: '4号楼'
    },
    {
        label: '5号楼',
        value: '5号楼'
    }
]
export default address;