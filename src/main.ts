/*
 * @Author: Badcandy 568197314@qq.com
 * @Date: 2022-12-18 14:43:05
 * @LastEditors: 晋龍
 * @LastEditTime: 2023-01-16 15:44:48
 * @FilePath: \dcollege\src\main.ts
 * @Description: 
 * 
 * Copyright (c) 2022 by Badcandy 568197314@qq.com, All Rights Reserved. 
 */
import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import '@/assets/font/font.css'//引入字体样式
import '@/assets/css/init.css'//取消页面边框margin与padding以及背景颜色
import Varlet from '@varlet/ui'
import '@varlet/ui/es/style.js'


const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(Varlet)
app.mount('#app')
